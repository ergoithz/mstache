# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[0.3.0]: https://gitlab.com/ergoithz/mstache/-/releases/0.3.0
[0.2.0]: https://gitlab.com/ergoithz/mstache/-/releases/0.2.0
[0.1.5]: https://gitlab.com/ergoithz/ustache/-/releases/0.1.5
[0.1.4]: https://gitlab.com/ergoithz/ustache/-/releases/0.1.4
[0.1.3]: https://gitlab.com/ergoithz/ustache/-/releases/0.1.3
[0.1.2]: https://gitlab.com/ergoithz/ustache/-/releases/0.1.2
[0.1.1]: https://gitlab.com/ergoithz/ustache/-/tags/0.1.1
[0.1.0]: https://gitlab.com/ergoithz/ustache/-/tags/0.1.0

## [0.3.0] - 2024-10-12

### Added
- `keep_lines` parameter to disable Mustache tag-only-line collapsing.
- `strict` parameter to disable non-standard `mustache.js` compatibility workarounds.
- `SectionException`, `UnclosedSectionException`, `UnopenedSectionException` exceptions.
- Support for variable-style lambdas (`{{lambda}}`).
- Support for Mustache-style single-argument lambdas (`lambda content: '{{var}}'`),
  pass `lambda_render=None` to enable.
- Support for [PEP-561](https://peps.python.org/pep-0561/) typing.

### Fixed
- Fix `None` variables being printed in the template.
- Fix delimiter changes (`{{= start end =}}`) propagating to partials.
- Fix support multispaced delimiters (`{{= start   end =}}`).
- Fix Mustache whitespace handling (pass `keep_lines=True` to disable).
- Fix partial (`{{>partial}}`) indentation (pass `keep_lines=True` to disable).
- Invalid extra delimiters (`{{= start end ignored =}}`) handled like `mustache.js`
  (pass `strict=True` to raise `DelimiterTokenException` instead).

### Changed
- **Breaking**: `tokenize` will now remove tag-only lines following the specification,
  `keep_lines` option added for whitespace-sensitive use-cases.
- **Breaking**: leaving a section unclosed now raises `UnclosedSectionException`
  instead of `UnclosedTokenException`.
- Closing an unknown section now raises `UnopenedSectionException`.
- `getter` argument (`render`, `stream`), additionally to current `PropertyGetter`,
  will now support `PropertyGetterTuple`, a tuple of two `PropertyGetter` functions
  specific for compat and strict modes.
- `getter` argument (`render`, `stream`) now defaults to `default_getter_tuple`
  instead of `default_getter`.
- `default_getter` scope argument now expects a sequence instead of an iterable.
- `UnopenedTokenException` raised instead of its `ClosingTokenException` base class.
- `PartialResolver` interface now allows returning `None`.
- `default_resolver` now returns `None` instead of empty bytes.
- `CacheMakeKeyFunction` argument `tuple` contains two extra `bool` items.

## [0.2.0] - 2024-05-21

### Added
- `cache_make_key` parameter enabling cache key customization.

### Fixed
- Fix custom stringify lost on lambdas and partials.
- Fix custom getter lost on lambdas and partials.
- Fix `±NaN` behaving as truthy ([#2](https://gitlab.com/ergoithz/mstache/-/issues/2)).
- Address constant mypy breaking changes.

### Changed
- Raised minimum python version to `3.10.0`.
- **Breaking**: `__bool__` is now honored except for mappings and `±NaN`.
- **Breaking**: objects implementing `__float__` can be considered `±NaN`.
- **Breaking**: internal `TokenException.message` renamed to `TokenException._fmt`.
- **Breaking**: tokenizer is no longer a generator function, returns a tuple now.
- **Breaking**: non-string attributes hacked onto `__dict__` are now unreachable.
- Drop optional `xxhash` dependency, `cache_make_key` should be used instead.
- Project rename.
- Optimizations.

## [0.1.5] - 2021-12-17

### Added
- Changelog.

### Changed
- Migrate tokenizer to an indexed balanced decision tree.
- Migrate renderer to a balanced decision tree.
- Other minor optimizations.

### Fixed
- Fix `tokenizer` raising `IndexError` with truncated tokens.

## [0.1.4] - 2021-04-13

### Fixed
- Fix longstanding issue with unclosed tags hanging the tokenizer.

## [0.1.3] - 2021-04-11

### Added
- `bytes` templates now support byte-keyed scope items and virtuals.
- `default_getter` now supports custom virtual properties.
- Optional `xxhash` support to minimize template cache memory footprint.

### Fixed
- Fix argument forwarding.
- Fix incorrect behavior with nested undefined properties.
- Fix default parameter values not being public.

## [0.1.2] - 2021-01-29

### Fixed
- Fix tokenizer raising `IndexError` when trying to close the top level scope.

## [0.1.1] - 2021-01-11

### Fixed
- Fix handling of whitespaced tokens.

## [0.1.0] - 2021-01-01

### Added
- Initial release.
