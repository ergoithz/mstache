
import collections
import collections.abc
import contextlib
import dataclasses
import decimal
import errno
import functools
import importlib
import importlib.util
import io
import itertools
import json
import os
import pathlib
import re
import sys
import tempfile
import threading
import typing
import unittest
import unittest.mock
import zipfile

import mstache


SPEC_URL = 'https://github.com/mustache/spec/archive/refs/heads/master.zip'
RE_SLUG = re.compile(r'[^A-Za-z0-9]+')
RE_SPEC_JSON = re.compile(r'''
    ^spec-master/specs/
    ([^~][^.]+|~lambdas)
    \.json$
    ''', re.VERBOSE)


@dataclasses.dataclass(frozen=True, kw_only=True)
class SpecTest:
    name: str
    template: str
    expected: str
    desc: str = ''
    partials: dict = dataclasses.field(default_factory=dict)
    data: typing.Any = None

    group: str
    group_index: int
    index: int


class SpecMeta(type):

    def _test(
            self,
            name: str,
            spec: SpecTest,
            dct: collections.abc.Mapping[str, typing.Any],
            *,
            strict: bool = False,
            ) -> None:

        def test(self: unittest.TestCase) -> None:
            with self.subTest(title):
                try:
                    result = mstache.render(
                        spec.template,
                        scope,
                        resolver=spec.partials.get,
                        lambda_render=None,
                        strict=strict,
                        )
                except:
                    self.fail(failmsg)

                self.assertEqual(result, expectation, failmsg)

        scope = spec.data
        if self._get(scope, 'lambda', '__tag__') == 'code':
            code = self._get(scope, 'lambda', 'python')
            if code not in dct.get('known_lambdas', ()):
                self._fail(name, f'Unkown lambda {code!r}')
                return
            scope = scope | {'lambda': eval(code, {})}

        title = f'{spec.group_index}.{spec.group} > {spec.index}.{spec.name}'
        compat = self._get(dct, 'compat_overrides', spec.group, spec.name)
        expectation = spec.expected if compat is None else compat
        if strict:
            title = f'{title} (strict)'
            expectation = spec.expected
        failmsg = f'{title}\n{spec!r}'
        setattr(self, name, test)

    def _fail(self, name: str, message: str) -> None:

        def test(self: unittest.TestCase) -> typing.NoReturn:
            self.fail(message)

        setattr(self, name, test)

    def _get(
            self,
            dct: collections.abc.Mapping,
            *path: collections.abc.Hashable,
            ) -> typing.Any:
        with contextlib.suppress(KeyError, IndexError, TypeError):
            for k in path:
                dct = dct[k]
            return dct
        return None

    def __init__(self, name: str, bases: list[type], dct: dict) -> None:
        path = pathlib.Path(os.getenv('MUSTACHE_SPEC_ZIP', 'spec-master.zip'))
        if not path.is_file():
            self._fail('test_spec', f'Mustache spec zip file {path} not found')
            return

        counter = itertools.count()
        with zipfile.ZipFile(path) as z:
            tests = {
                match.group(1): json.loads(z.read(info))
                for info in z.infolist()
                if (match := RE_SPEC_JSON.match(info.filename))
                }

        known_lambdas = frozenset(dct.get('known_lambdas', ()))
        for g, (stem, data) in enumerate(tests.items()):
            for t, test in enumerate(data['tests']):
                slug = (
                    RE_SLUG.sub('_', f'test_{stem}_{test["name"]}')
                    .rstrip('_')
                    .lower()
                    )
                spec = SpecTest(
                    **test,
                    index=t,
                    group=stem,
                    group_index=g,
                    )
                self._test(f'{slug}_strict', spec, dct, strict=True)
                self._test(slug, spec, dct)


class TestRender(unittest.TestCase):
    lock = threading.Lock()

    def tearDown(self) -> None:
        super().tearDown()
        mstache.default_cache.clear()

    def assertRender(self, template, variables, expectation, msg=None, **kwargs):
        with self.subTest(f'{template!r}, {variables!r}'):
            result = mstache.render(template, variables, **kwargs)
            self.assertEqual(expectation, result, msg)

    def assertTruthy(self, scope, msg=None, **kwargs):
        msg = msg or f'{scope!r} is not truthy'
        self.assertRender('{{#.}}truthy{{/.}}', scope, 'truthy', msg, **kwargs)

    def assertFalsy(self, scope, msg=None, **kwargs):
        msg = msg or f'{scope!r} is not falsy'
        self.assertRender('{{^.}}falsy{{/.}}', scope, 'falsy', msg, **kwargs)

    def run_module(self, module, argv=None, stdin=''):
        argv = [module.__name__] if argv is None else argv
        with (self.lock,
              io.StringIO(stdin) as stdin,
              io.StringIO() as stdout,
              io.StringIO() as stderr,
              unittest.mock.patch.object(sys, 'stdin', stdin),
              unittest.mock.patch.object(sys, 'stdout', stdout),
              unittest.mock.patch.object(sys, 'stderr', stderr),
              unittest.mock.patch.object(sys, 'argv', argv)):
            try:
                loader = type(module.__loader__)('__main__', module.__file__)
                spec = importlib.util.spec_from_loader(loader.name, loader)
                loader.exec_module(importlib.util.module_from_spec(spec))
                code = 0
            except SystemExit as e:
                code = e.code
            return code, stdout.getvalue(), stderr.getvalue()

    def validate(self, template, **kwargs):
        data: bytes = (
            template.encode()  # type: ignore
            if isinstance(template, str) else
            template
            )
        mstache.tokenize(data, cache={}, **kwargs)

    def test_stream(self):
        class A:
            world = 'World!'
            bworld = b'World!'

        variables = {'world': 'World!'}

        template = 'Hello {{world}}'
        expectation = ''.join(mstache.stream(template, variables))
        self.assertRender(template, variables, expectation)

        template = b'Hello {{world}}'
        expectation = b''.join(mstache.stream(template, variables))
        self.assertRender(template, variables, expectation)

        variables = {b'world': 'World!'}
        expectation_b = b''.join(mstache.stream(template, variables))
        self.assertRender(template, variables, expectation)
        self.assertRender(template, variables, expectation_b)

        variables = {b'world': b'World!'}
        expectation_b = b''.join(mstache.stream(template, variables))
        self.assertRender(template, variables, expectation)
        self.assertRender(template, variables, expectation_b)

        self.assertRender('Hello {{world}}', {}, 'Hello ')
        self.assertRender('Hello {{0}}', ['World!'], 'Hello World!')
        self.assertRender('Hello {{0}}', [], 'Hello ')
        self.assertRender('Hello {{a.length}}', {'a': []}, 'Hello 0')
        self.assertRender('Hello {{world}}', A, 'Hello World!')
        self.assertRender('Hello {{bworld}}', A, 'Hello b&#x60;World!&#x60;')

    def test_render(self):
        class A:
            world = 'World!'
            bworld = b'World!'

        variables = {'world': 'World!'}
        self.assertRender('Hello {{world}}', variables, 'Hello World!')
        self.assertRender(b'Hello {{world}}', variables, b'Hello World!')

        variables = {b'world': 'World!'}
        self.assertRender(b'Hello {{world}}', variables, b'Hello World!')

        variables = {b'world': b'World!'}
        self.assertRender(b'Hello {{world}}', variables, b'Hello World!')

        variables = {'world': b'World!'}
        self.assertRender(b'Hello {{world}}', variables, b'Hello World!')

        self.assertRender(b'Hello {{world}}', {}, b'Hello ')
        self.assertRender(b'Hello {{0}}', [b'World!'], b'Hello World!')
        self.assertRender(b'Hello {{0}}', [], b'Hello ')
        self.assertRender(b'Hello {{a.length}}', {b'a': []}, b'Hello 0')
        self.assertRender(b'Hello {{world}}', A, b'Hello World!')
        self.assertRender(b'Hello {{bworld}}', A, b'Hello World!')

    def test_block(self):
        self.assertRender(
            '{{#falsy}}truthy{{/falsy}}{{^falsy}}falsy{{/falsy}}',
            {'falsy': False},
            'falsy',
            )
        self.assertRender(
            '{{#truthy}}truthy{{/truthy}}{{^truthy}}falsy{{/truthy}}',
            {'truthy': True},
            'truthy',
            )
        self.assertRender(
            '{{#obj}}{{a}}{{b}}{{c}}{{d.a}}{{d.b}}{{/obj}}',
            {'obj': {'a': 0, 'b': 1, 'd': {'a': 3}}, 'c': 2, 'd': {'b', 4}},
            '0123',
            )
        self.assertRender(
            '{{#obj}}{{a}}{{b.a}}{{/obj}}',
            {'obj': type('O', (), {'a': 0, 'b': {'a': 1}})()},
            '01',
            )
        self.assertRender(
            '{{#a}}{{.}}{{/a}}{{#b}}{{.}}{{/b}}{{#c}}{{.}}{{/c}}',
            {'a': [1, 2, 3], 'b': 4, 'c': [5, 6]},
            '123456',
            )
        self.assertRender(
            '{{#array}}{{#0}}{{.}}{{/0}}{{^0}}0{{/0}}{{^.}}0{{/.}}{{/array}}',
            {'array': [[1, 2, 3], []]},
            '100',
            )
        self.assertRender(
            '{{#exhausted}}hello{{/exhausted}}',
            {'exhausted': iter(())},
            '',
            )

    def test_whitespace(self):
        self.assertRender(
            '{{#block}}\nhello\n{{/block}}\n',
            {'block': True},
            'hello\n',
            )
        self.assertRender(
            '{{#block}}\nhello\n{{/block}}\n',
            {'block': True},
            '\nhello\n\n',
            keep_lines=True,
            )

    def test_truthness(self):
        inf = float('inf')
        values = True, b'\0', 1, .1, inf, -inf, {}, object()
        for x in values:
            self.assertTruthy(x)

    def test_falseness(self):
        nan = float('nan')
        dnan = decimal.Decimal('nan')
        values = False, None, '', 0, .0, -.0, nan, -nan, dnan, -dnan, set(), []
        for x in values:
            self.assertFalsy(x)

    def test_lambda_block(self):
        with self.subTest('explicit render'):
            self.assertRender(
                '{{#lambda}}hello{{/lambda}}',
                {'lambda': lambda t, render: render(f'lambda:{t}')},
                'lambda:hello',
                )
            self.assertRender(
                '{{#lambda}}hello{{/lambda}}',
                {'lambda': lambda t, render: None},
                '',
                )

        with self.subTest('implicit render'):
            self.assertRender(
                '{{#lambda}}hello{{/lambda}}',
                {'lambda': lambda t: f'lambda:{{{{{t}}}}}', 'hello': 'value'},
                'lambda:value',
                lambda_render=None,
                )
            self.assertRender(
                '{{#lambda}}hello{{/lambda}}',
                {'lambda': lambda t: 1},
                '1',
                lambda_render=None,
                )
            self.assertRender(
                '{{#lambda}}hello{{/lambda}}',
                {'lambda': lambda t: None},
                '',
                lambda_render=None,
                )

    def test_lambda_variable(self):
        self.assertRender(
            '{{lambda}}',
            {'lambda': lambda: 'hello'},
            'hello',
            )
        self.assertRender(
            '{{lambda}}',
            {'lambda': lambda: '{{value}}', 'value': 'hello'},
            'hello',
            )
        self.assertRender(
            '{{lambda}}',
            {'lambda': lambda: None},
            '',
            )

    def test_properties(self):
        self.assertRender('{{value}}{{missing}}', {'value': 'value'}, 'value')

        self.assertRender('{{{value}}}', {'value': b'value'}, 'b\'value\'')
        self.assertRender('{{{value}}}', {b'value': b'value'}, '')
        self.assertRender('{{{arr.length}}}', {'arr': []}, '0')
        self.assertRender('{{{arr.length}}}', {b'arr': []}, '')

        self.assertRender(b'{{{value}}}', {'value': b'value'}, b'value')
        self.assertRender(b'{{{value}}}', {b'value': b'value'}, b'value')
        self.assertRender(b'{{{arr.length}}}', {'arr': []}, b'0')
        self.assertRender(b'{{{arr.length}}}', {b'arr': []}, b'0')

        self.assertRender(b'{{{\xff}}}', {b'\xff': 1}, b'1')
        self.assertRender(b'{{{\xff}}}', {}, b'')

        self.assertRender(
            '{{#scope}}{{variable}}{{/scope}}',
            {'scope': {'variable': 'hello'}},
            'hello',
            )
        self.assertRender(
            '{{#}}{{}}{{/}}',
            {'': 'value'},
            'value',
            )
        self.assertRender(
            '{{# scope}}{{ a }}{{ b}}{{/ scope }}',
            {'scope': {'a': 'a', 'b': 'b'}},
            'ab',
            )
        self.assertRender(
            '{{array.0}}{{array.length}}{{obj.2}}{{obj.3}}{{obj.length}}',
            {'array': [0], 'obj': {2: 2, '3': 3}},
            '0123',
            )
        self.assertRender(
            '{{#nested}}{{prop.value}}{{/nested}}',
            {'prop': {'value': 'value'}, 'nested': {'prop': {}}},
            'value',
            )

    def test_partial(self):
        template = '{{#scope}}{{>partial}}{{/scope}}'
        variables = {'scope': 'hello'}
        partials = {'partial': '{{.}}'}
        self.assertRender(template, variables, '')
        self.assertRender(template, None, '', resolver=partials.get)
        self.assertRender(template, variables, 'hello', resolver=partials.get)

    def test_unescaped(self):
        self.assertRender('{{.}}', '<>', '&lt;&gt;')
        self.assertRender('{{&.}}', '<>', '<>')
        self.assertRender('{{{.}}}', '<>', '<>')

    def test_comment(self):
        self.assertEqual(
            mstache.tokenize(b'{{! comment }}'),
            mstache.tokenize(b''),
            )
        self.assertNotEqual(
            mstache.tokenize(b'{{! comment }}', comments=True),
            mstache.tokenize(b''),
            )
        verbose_tokenize = functools.partial(mstache.tokenize, comments=True)
        with unittest.mock.patch.object(mstache, 'tokenize', verbose_tokenize):
            self.assertRender('{{! comment }}', None, '')

    def test_tags(self):
        self.assertRender(
            '{{=<m: />=}}<b><m:#obj/><m:value/><m:/obj/><br/>world</b>',
            {'obj': {'value': 'hello'}},
            '<b>hello<br/>world</b>',
            )

        with self.assertRaises(mstache.DelimiterTokenException):
            self.validate(b'{{= =}}')

        with self.assertRaises(mstache.DelimiterTokenException):
            self.validate(b'{{=a =}}')

        with self.assertRaises(mstache.DelimiterTokenException):
            self.validate('{{= a b c =}}acb', strict=True)

        self.assertRender('{{= a b c =}}acb', {'c': 'hello'}, 'hello')

    def test_silent(self):
        self.assertRender(
            '{{#missing}}{{#a}}{{.}}{{/a}}{{#b}}{{.}}{{/b}}{{/missing}}',
            {'a': 1, 'b': [1, 2, 3]},
            '',
            )

    def test_unopened_section_exception(self):
        broken = (
            b'{{#block}}{{/unmatching}}',
            b'{{/}}',
            )
        for case in broken:
            with self.subTest(case), \
                 self.assertRaises(mstache.UnopenedSectionException):
                self.validate(case)

    def test_unclosed_section_exception(self):
        unclosed = (
            b'{{#unclosed}}',
            b' {{#unclosed}}',
            b'{{#unclosed}} ',
            )
        for case in unclosed:
            with (self.subTest(case),
                  self.assertRaises(mstache.UnclosedSectionException)):
                self.validate(case)

    def test_unclosed_token_exception(self):
        unclosed = (
            b'{{',
            b' {{ unclosed',
            b'{{#unclosed} ',
            )
        for case in unclosed:
            with (self.subTest(case),
                  self.assertRaises(mstache.UnclosedTokenException)):
                self.validate(case)

    def test_cache(self):
        maxsize = 10
        cache = mstache.LRUCache(maxsize)
        identity = object()

        def keyerror(*args, **kwargs):
            raise KeyError

        with unittest.mock.patch.object(cache, 'move_to_end', keyerror):
            cache['a'] = 1
            self.assertRaises(KeyError, cache.__getitem__, 'a')
            self.assertIs(cache.get('a', identity), identity)

        with unittest.mock.patch.object(cache, 'popitem', keyerror):
            cache.update((i, i) for i in range(maxsize + 1))

    def test_recursion_limit(self):
        templates = {
            'base': ('Hello World!', 0),
            'level_1': ('{{>base}}', 1),
            'level_2': ('{{>level_1}}', 2),
            }
        for case, (template, level) in templates.items():
            render = functools.partial(
                mstache.render,
                template,
                None,
                resolver=lambda k: templates[k][0],
                )
            with self.subTest(case):
                render(recursion_limit=level)
                self.assertRaises(
                    mstache.TemplateRecursionError,
                    render,
                    recursion_limit=level - 1,
                    )

    def test_virtuals(self):
        class A:
            _name = 'A name'
            _attr = b'A attr'

        kwargs = {
            'getter': functools.partial(
                mstache.default_getter,
                virtuals={
                    **mstache.default_virtuals,
                    'name': lambda x: x._name,
                    },
                )
            }
        self.assertRender('{{name}}', A, 'A name', **kwargs)
        self.assertRender(b'{{name}}', A, b'A name', **kwargs)

        self.assertRender('{{length}}', [], '0', **kwargs)
        self.assertRender('{{length}}', {}, '', **kwargs)
        self.assertRender('{{length}}', {'length': 'a'}, 'a', **kwargs)
        self.assertRender(b'{{length}}', {'length': 'a'}, b'a', **kwargs)

    def test_cli(self):
        template = '{{#truthy}}\nHello {{world}}{{/truthy}}'
        scope = {'world': 'World!', 'truthy': True}
        expectation = 'Hello World!'
        keep_expectation = '\nHello World!'

        with contextlib.ExitStack() as stack:
            t = stack.enter_context(
                tempfile.NamedTemporaryFile('w', suffix='.mustache'),
                )
            t.write(template)
            t.flush()

            with self.subTest('-'):
                code, stdout, stderr = self.run_module(
                    mstache,
                    ['mstache', t.name],
                    json.dumps(scope),
                    )
                self.assertFalse(code)
                self.assertEqual(stdout, expectation)
                self.assertFalse(stderr)

            with self.subTest('-k'):
                code, stdout, stderr = self.run_module(
                    mstache,
                    ['mstache', t.name, '-k'],
                    json.dumps(scope),
                    )
                self.assertFalse(code)
                self.assertEqual(stdout, keep_expectation)
                self.assertFalse(stderr)

            d = stack.enter_context(
                tempfile.NamedTemporaryFile('w', suffix='.json'),
                )
            json.dump(scope, d)
            d.flush()

            with self.subTest('-j'):
                o = stack.enter_context(
                    tempfile.NamedTemporaryFile('w', suffix='.output'),
                    )
                code, stdout, stderr = self.run_module(
                    mstache,
                    ['mstache', t.name, '-j', d.name, '-o', o.name],
                    )
                self.assertFalse(code)
                self.assertFalse(stdout)
                self.assertFalse(stderr)
                self.assertEqual(pathlib.Path(o.name).read_text(), expectation)


class TestSpec(unittest.TestCase, metaclass=SpecMeta):
    compat_overrides = {
        'interpolation': {
            # Mustache.js behavior (see reference.comparison)
            'Dotted Names - Context Precedence': 'ERROR',
            }
        }
    known_lambdas = {
        'lambda: "world"',
        'lambda: "{{planet}}"',
        'lambda: "|planet| => {{planet}}"',
        'lambda: globals().update(calls=globals().get("calls",0)+1) or calls',
        'lambda: ">"',
        'lambda text: text == "{{x}}" and "yes" or "no"',
        'lambda text: "%s{{planet}}%s" % (text, text)',
        'lambda text: "%s{{planet}} => |planet|%s\" % (text, text)',
        'lambda text: "__%s__" % (text)',
        'lambda text: 0',
        }


if __name__ == '__main__':
    unittest.main()
