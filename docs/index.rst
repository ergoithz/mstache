.. title:: Index

Documentation
=============

Welcome to mstache documentation.

mstache, Mustache for Python
----------------------------

**mstache** is a `Mustache template language`_ implementation for Python,
focused on high compatibility with `mustache.js`_,
with no dependencies other than the `Python Standard Library`_.

.. _mustache: https://mustache.github.io/
.. _mustache_js: https://github.com/janl/mustache.js/
.. _stdlib: https://docs.python.org/3/library/index.html
.. _Mustache template language: mustache_
.. _mustache.js: mustache_js_
.. _Python Standard Library: stdlib_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   License <LICENSE>
   Readme <README>
   Templace cache <cache>
   Lambdas <lambdas>
   Strict mode <strictness>
   Non-HTML use-cases <nonhtml>
   Tips and tricks <tips>
   Changelog <CHANGELOG>
   API reference <mstache>
   External: Mustache manual <https://mustache.github.io/mustache.5.html>


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
