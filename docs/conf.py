"""
Configuration file for the Sphinx documentation builder.

Reference: https://www.sphinx-doc.org/en/master/usage/configuration.html

Copyright (c) 2021-2024, Felipe A Hernandez.

MIT License (see `LICENSE`_).

.. _LICENSE: https://gitlab.com/ergoithz/mstache/-/blob/master/LICENSE

"""

import mstache

project = mstache.__name__
copyright = f'2021-2024, {mstache.__author__}'  # noqa
author = mstache.__author__
release = mstache.__version__
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.coverage',
    'sphinx.ext.intersphinx',
    'sphinx.ext.autosectionlabel',
    'sphinx_autodoc_typehints',
    'myst_parser',
    ]
templates_path = ['_templates']
exclude_patterns = []
html_theme = 'alabaster'
html_static_path = ['_static']
html_sidebars = {
    '**': [
        'sidebar.html',
        'globaltoc.html',
        'relations.html',
        'sourcelink.html',
        'searchbox.html',
        ],
    }
html_css_files = [
    'custom.css',
    ]
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
    '.txt': 'markdown',
    }
autodoc_default_options = {
    'members': True,
    'undoc-members': False,
    'no-undoc-members': True,
    # 'private-members': ','.join(()),
    'special-members': '__setitem__',   # ','.join(())
    'inherited-members': False,
    'show-inheritance': True,
    'member-order': 'bysource',
    # 'exclude-members': ','.join(()),
    }
intersphinx_mapping = {
    'python': ('https://docs.python.org/', None),
    }
autosectionlabel_prefix_document = True
