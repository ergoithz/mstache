Strict mode
-----------

`Mustache v1.4.2 spec`_ defines couple of rules often ignored by specific
implementations like `mustache.js`_ for which we aim maximum compatibility
with.

.. _mustache_spec: https://github.com/mustache/spec/releases/tag/v1.4.2
.. _mustache_js: https://github.com/janl/mustache.js/
.. _Mustache v1.4.2 spec: mustache_spec_
.. _mustache.js: mustache_js_

To match both use-cases, **mstache** follows `mustache.js`_ implementation
quirks while also providing an ``strict`` toggle to enable the strict behavior.

The strict behavior, enabled by passing ``strict=True`` to :func:`mstache.render`,
:func:`mstache.stream`, :func:`mstache.tokenize`, implies:

- Setting invalid delimiters containing spaces will raise :class:`mstache.DelimiterTokenException`
  instead of undefined behavior.
- If `getter` parameter is an :py:attr:`mstache.PropertyGetterTuple` (default),
  its second item, :func:`mstache.default_strict_getter`, will be used.

.. caution::
   Strict-mode is expected to implement more syntax-related exceptions over time,
   either because of spec updates or more quirks being discovered.

   Use strict mode with caution for when strictness is desirable (like validation).

Strict getter
=============

The stricter getter from :py:attr:`mstache.PropertyGetterTuple` second element
considers a nested reference (`nested.value`) to be missing on partial matches,
that is, when it does not completely resolve but the parent component of
the reference is in the scope.

This is the intended behavior defined by `Mustache v1.4.2 spec`_.

Example: non-strict mode
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    import mstache
    mstache.render(
        '{{#a}}{{b.c}}{{/a}}',
        {'a': {'b': {}}, 'b': {'c': 1}},
        )
    # '1'

Explained as:

1. Under scope ``a``, variable ``b`` has no ``c`` key.
2. Under root scope, deep reference to ``b.c`` has a value.

Example: strict mode
~~~~~~~~~~~~~~~~~~~~

.. code:: python

    import mstache
    mstache.render(
        '{{#a}}{{b.c}}{{/a}}',
        {'a': {'b': {}}, 'b': {'c': 1}},
        strict=False,
        )
    # '' (empty string)

Explained as:

1. Under scope ``a``, variable ``b`` has no ``c`` key.
2. Since ``b`` was partially matched, ``b.c`` is considered missing.
