const
  fs = require('fs'),
  mustache = require('mustache'),
  tests = JSON.parse(fs.readFileSync(process.argv[2]));

console.log(JSON.stringify(tests.map(
  ([name, template, values, partials]) => {
    try {
      return mustache.render(
        template,
        Object.assign(values, {
          lambda: () => (template, render) => render(template),
          error: () => { throw new Error('error') },
          nan: NaN,
        }),
        partials,
      );
    } catch (e) {
      return {$: String(e)};
    }
  },
)));
