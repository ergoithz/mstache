
import os
import pathlib
import platform
import textwrap
import timeit

import tabulate

import mstache

try:
    import chevron
except ImportError:
    chevron = None

jit = platform.python_implementation() in ('PyPy', 'Pyston')
template = textwrap.dedent('''
    variable: {{value}}
    escaped: {{literal}}
    unescaped: {{{literal}}} and {{&literal}}
    block: {{#nested}}{{value}} and {{#lambda}}{{value}}{{/lambda}}{{/nested}}
    partial on values: {{#values}}{{>partial}}{{/values}}
    missing partial: {{>missing}}
    {{=(( ))=}}tag switch: {{Hello ((world))}}((={{ }}=))
    --- {{!
        we do things faster
        }}
    values: {{#values}}{{.}}{{/values}}{{^values}}falsy{{/values}}
    values.0: {{#values.0}}{{.}}{{/values.0}}{{^values.0}}falsy{{/values.0}}
    empty: {{#empty}}{{.}}{{/empty}}{{^empty}}falsy{{/empty}}
    empty.0: {{#empty.0}}{{.}}{{/empty.0}}{{^empty.0}}falsy{{/empty.0}}
    {{.}}
    {{#arrays}}[{{#.}}{{.}}{{/.}}] is {{^.}}empty{{/.}}{{#0}}non empty{{/0}}
    {{#arrays_many}}{{#.}}{{.}}{{/.}}{{/arrays_many}}
    {{/arrays}}
    ''').strip()
template_bytes = template.encode()
values = {
    'world': 'World!',
    'value': 'value',
    'nested': {
        'value': 'nested value',
        },
    'values': [1, 2, 3],
    'arrays': [[1, 2, 3], []],
    'arrays_many': [[1, 2, 3]] * 20,
    'literal': '<!-- literal -->',
    'empty': [],
    'lambda': lambda template, render: render(
        'lambda:{!r}'.format(
            template.decode() if isinstance(template, bytes) else
            template
            )),
    }
partials = {
    'partial': 'partial:{{.}} ',  # text template
    b'partial': b'partial:{{.}} ',  # bytes template
    }
resolver = partials.get
number = 1000

mstache_tests = {
    'str': lambda m, o: m.render(
        template,
        values,
        resolver=resolver,
        **o,
        ),
    'str, no cache': lambda m, o: m.render(
        template,
        values,
        cache={},
        resolver=resolver,
        **o,
        ),
    'stream, str': lambda m, o: ''.join(m.stream(
        template,
        values,
        resolver=resolver,
        **o,
        )),
    'stream, str, no cache': lambda m, o: ''.join(m.stream(
        template,
        values,
        cache={},
        resolver=resolver,
        **o,
        )),
    'bytes': lambda m, o: m.render(
        template_bytes,
        values,
        resolver=resolver,
        **o,
        ),
    'bytes, no cache': lambda m, o: m.render(
        template_bytes,
        values,
        cache={},
        resolver=resolver,
        **o,
        ),
    'stream, bytes': lambda m, o: b''.join(m.stream(
        template_bytes,
        values,
        resolver=resolver,
        **o,
        )),
    'stream, bytes, no cache': lambda m, o: b''.join(m.stream(
        template_bytes,
        values,
        cache={},
        resolver=resolver,
        **o,
        )),
    }
chevron_tests = {
    'str': lambda m, o: m.render(
        template,
        values,
        partials_dict=partials,
        **o,
        ),
    }
module_tests = (
    (mstache, {}, mstache_tests),
    (mstache, {'strict': True}, mstache_tests),
    (mstache, {'keep_lines': True}, mstache_tests),
    (mstache, {'keep_lines': True, 'strict': True}, mstache_tests),
    (chevron, {}, chevron_tests),
    )


def runtime(module, options, test):
    if test:
        for _ in range(number if jit else 1):  # warm up jit
            test(module, options)
        return timeit.timeit(lambda: test(module, options), number=number)
    return None


if __name__ == '__main__':
    keys = {k: None for *_, d in module_tests for k in d}
    out = tabulate.tabulate({f'test (x{number})': keys} | {
        ' '.join((m.__name__, *o)): [runtime(m, o, t.get(k)) for k in keys]
        for m, o, t in module_tests if m
        }, headers='keys', tablefmt='grid')

    out = '.. rst-class:: wide\n\n' + textwrap.indent(out, '   ')

    if p := os.getenv('RST_OUTPUT'):
        pathlib.Path(p).write_text(out)

    print(out)
