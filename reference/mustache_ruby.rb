require 'mustache'
require 'json'

class Patchstache < Mustache

  def self.render(*args)
    new.render_with_partials(*args)
  end

  def render_with_partials(data = template, ctx = {}, partials = {})
    @partials = partials
    render(data, ctx)
  end

  def partial(name)
    @partials[name.to_sym]
  end
end

fnc = Proc.new do |template|
  template
end

err = Proc.new do
  raise Exception.new "error"
end

tests = JSON.parse(File.read(ARGV[0]))
puts tests.map {|(name, tpl, data, partials)|
  begin
    Patchstache.render(
      tpl,
      data.merge({ "lambda" => fnc, "error" => err, "nan": Float::NAN }),
      partials
      )
  rescue Exception => e
    { "$" => "#{e.class}: #{e.message}" }
  end
}.to_json
