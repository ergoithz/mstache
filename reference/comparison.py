
import collections.abc
import functools
import importlib.resources
import importlib.resources.abc
import json
import logging
import os
import pathlib
import subprocess
import tempfile
import textwrap

import tabulate

import mstache
import chevron

JSONObject = dict[str, 'JSON']
JSON = str | int | float | bool | list['JSON'] | JSONObject | None
Test = tuple[str, str, JSONObject, dict[str, str]]
TestRunner = collections.abc.Callable[[collections.abc.Iterable[Test]], list[JSON]]

files = importlib.resources.files(__package__)
dumps = json.dumps
xkeys = {
    'lambda': None,
    'error': None,
    'nan': None,
    }

def run_script(
        command: str,
        script: importlib.resources.abc.Traversable,
        data: collections.abc.Iterable[Test],
        ) -> list[JSON]:
    with tempfile.TemporaryDirectory() as p:
        tgt = pathlib.Path(p, 'data.json')
        tgt.write_text(dumps([
            (name, template, context | xkeys, partials)
            for name, template, context, partials in data
            ]))
        src = pathlib.Path(p, script.name)
        src.write_bytes(script.read_bytes())
        out = subprocess.check_output((command, str(src), str(tgt)), text=True)
        return json.loads(out)

def raise_exc():
    raise Exception('error')

def wrap_exc(fnc: collections.abc.Callable, *args, **kwargs) -> JSON:
    try:
        return fnc(*args, **kwargs)
    except Exception as e:
        return {'$': repr(e)}

def run_mstache(tests: collections.abc.Iterable[Test], **kwargs) -> list[JSON]:
    ctx = xkeys | {
        'lambda': lambda template: template,
        'error': raise_exc,
        'nan': float('nan'),
        }
    return [
        wrap_exc(
            mstache.render, template, context | ctx,
            resolver=partials.get, **kwargs,
            )
        for _, template, context, partials in tests
        ]

def run_chevron(tests: collections.abc.Iterable[Test]) -> list[JSON]:
    ctx = xkeys | {
        'lambda': lambda template, render: render(template),
        'error': raise_exc,
        'nan': float('nan'),
        }
    return [
        wrap_exc(
            chevron.render, template, context | ctx,
            partials_dict=partials
            )
        for _, template, context, partials in tests
        ]

def run_mustache_js(tests: collections.abc.Iterable[Test]) -> list[JSON]:
    return run_script('node', files / 'mustache_node.js', tests)

def run_mustache_rb(tests: collections.abc.Iterable[Test]) -> list[JSON]:
    return run_script('ruby', files / 'mustache_ruby.rb', tests)

def run_suite(name: str, runner: TestRunner, tests: collections.abc.Sequence[Test]) -> list[str | None]:
    try:
        return [
            escape(item['$'] if isinstance(item, dict) else repr(item))
            for item in runner(tests)
            ]
    except (FileNotFoundError, subprocess.CalledProcessError):
        logging.exception(f'{name} failed')
        return [None] * len(tests)

def escape(txt: str) -> str:
    return txt.replace('\n', r'\n').replace('\\', r'\\')

if __name__ == '__main__':
    runners = {
        'mstache': run_mstache,
        'mstache (strict)': functools.partial(run_mstache, strict=True),
        'mstache (keep lines)': functools.partial(run_mstache, keep_lines=True),
        'chevron': run_chevron,
        'mustache (node)': run_mustache_js,
        'mustache (ruby)': run_mustache_rb,
        }
    suite = (
        ('Array access',
         '{{a.0}}',
         {'a': [1]},
         {}),
        ('Object truthyness',
         '{{#a}}1{{/a}}',
         {'a': {}},
         {}),
        ('NaN truthyness',
         '{{#nan}}1{{/nan}}',
         {'nan': float('NaN')},
         {}),
        ('Non-accessible keys',
         '{{a}}{{ b }}{{c.c}}',
         {'a': 'a', ' b ': 'b', 'c.c': 'c'},
         {}),
        ('Block lines',
         '{{#a}}\na\n{{/a}}\n',
         {'a': True},
         {}),
        ('Exceptions from lambda',
         'a{{error}}b',
         {'error': raise_exc},
         {}),
        ('Template unclosed error',
         '{{#broken}}',
         {},
         {}),
        ('Template unopened error',
         '{{/broken}}',
         {},
         {}),
        ('Invalid tags content',
         '{{= a b c =}}a a b c',
         {'a': 1, 'a b': 2},
         {}),
        ('Empty tags content',
         '{{=  =}}',
         {},
         {}),
        ('Spec: interpolation - Dotted Names - Context Precedence',
         '{{#a}}{{b.c}}{{/a}}',
         {'a': {'b': {}}, 'b': {'c': 'ERROR'}},
         {}),
        )

    results = [{} for t, *_ in suite]
    for name, runner in runners.items():
        for res, item in zip(results, run_suite(name, runner, suite), strict=True):
            res[name] = item

    fmt = (
        '{title}\n~~~~\n\n'
        '.. code:: handlebars\n\n   {template}\n\n'
        '.. code:: python\n\n   {context}\n\n'
        '.. rst-class:: wide\n\n'
        '{table}'
        ).format
    out = '\n\n'.join(
        fmt(
            title=h,
            template=escape(t),
            context=repr(c),
            table=textwrap.indent(
                tabulate.tabulate(
                    {'library': list(r), 'result': list(r.values())},
                    headers='keys',
                    tablefmt='grid',
                    ),
                '   ',
                ),
            )
        for (h, t, c, _), r in zip(suite, results, strict=True)
        )

    if p := os.getenv('RST_OUTPUT'):
        pathlib.Path(p).write_text(out)

    print(out)
